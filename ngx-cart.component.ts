import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-cart',
  template: require('pug-loader!./ngx-cart.component.pug')(),
  styleUrls: ['./ngx-cart.component.scss']
})
export class NgxCartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
