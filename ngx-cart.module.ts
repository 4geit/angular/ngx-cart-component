import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxCartComponent } from './ngx-cart.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    NgxCartComponent
  ],
  exports: [
    NgxCartComponent
  ]
})
export class NgxCartModule { }
