import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxCartComponent } from './ngx-cart.component';

describe('cart', () => {
  let component: cart;
  let fixture: ComponentFixture<cart>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ cart ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(cart);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
